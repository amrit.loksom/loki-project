#!/bin/bash
set -e

## Update 
echo "Installing UPDATES"
# Install system updates
apt-get update -y
apt-get install -y vim

apt-get autoremove

###
# apt-get install -y build-essential snapd software-properties-common python3-pip procps curl file git tasksel
# for a in code firefox vlc; do snap install --classic $a;done

echo "vagrant ALL=(ALL) NOPASSWD:ALL" | tee -a /etc/sudoers

## Install GUI
apt-get install -y ubuntu-desktop
sleep 10
# change network rendering config
FILE=/etc/netplan/50-vagrant.yaml
if [ -f "$FILE" ]; then
    sed -i 's/networkd/NetworkManager/g' $FILE
    netplan apply
else 
    echo "$FILE does not exist."
fi

echo "Setting up ssh config"
# touch /home/vagrant/.ssh/config
cat <<EOF | tee /home/vagrant/.ssh/config
Host *
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null
EOF

## set up ssh config
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sed -i 's/^#AuthorizedKeysFile/AuthorizedKeysFile/g' /etc/ssh/sshd_config
sed -i '/^#PermitRootLogin/c\PermitRootLogin yes' /etc/ssh/sshd_config
sed -i 's/^#PubkeyAuthentication/PubkeyAuthentication/g' /etc/ssh/sshd_config
systemctl restart sshd 
