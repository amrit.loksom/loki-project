#!/bin/bash
set -e

## Update 
echo "Installing SNAP Packages"
apt-get update -y
apt-get install -y snapd
snap install --classic code
snap install vlc 
