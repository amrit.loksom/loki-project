#!/bin/bash
set -e

## Update 
echo "Installing AWS Packages"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
rm -rf aws awscliv2.zip

pip3 install boto3
