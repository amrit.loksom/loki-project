#!/bin/bash
set -e

apt update -y 
apt-get install -y \
    x11-xserver-utils \
    language-pack-en

### Configure auto login for vagrant user
cat <<'EOF' | tee /etc/gdm3/custom.conf
[daemon]
# Enabling automatic login
AutomaticLoginEnable = true
AutomaticLogin = vagrant
EOF

# set time to Europe/London
timedatectl set-timezone Europe/London 

### Change system language to English GB
cat <<'EOF' | tee /etc/default/locale
LANG=en_GB.UTF-8
EOF

cat <<'EOF' | tee /home/vagrant/.pam_environment
LANGUAGE        DEFAULT=en_GB:en
LANG    DEFAULT=en_GB.UTF-8
LC_NUMERIC      DEFAULT=en_GB.UTF-8
LC_TIME DEFAULT=en_GB.UTF-8
LC_MONETARY     DEFAULT=en_GB.UTF-8
LC_PAPER        DEFAULT=en_GB.UTF-8
LC_NAME DEFAULT=en_GB.UTF-8
LC_ADDRESS      DEFAULT=en_GB.UTF-8
LC_TELEPHONE    DEFAULT=en_GB.UTF-8
LC_MEASUREMENT  DEFAULT=en_GB.UTF-8
LC_IDENTIFICATION       DEFAULT=en_GB.UTF-8
PAPERSIZE       DEFAULT=a4
EOF

echo "Setting keyboard layout"
# set keyboard layout to GB
cat <<'EOF' | tee /etc/default/keyboard
XKBLAYOUT=gb
BACKSPACE=guess
EOF
echo "setxkbmap gb" >> /home/vagrant/.bashrc

### set vim as default system editor
su - vagrant -c "$(/usr/bin/update-alternatives --set vi /usr/bin/vim.basic)"
# echo "update-alternatives --set vi /usr/bin/vim.basic" >> /home/vagrant/.bashrc

# add vmware guest tools
# apt-get autoremove -y open-vm-tools 
# apt-get install -y open-vm-tools-desktop
