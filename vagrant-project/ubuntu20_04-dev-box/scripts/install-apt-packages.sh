#!/bin/bash
set -e

## Update 
echo "Installing APT Packages"
apt-get dist-upgrade -y 
apt-get update -y
apt-get install -y \
    build-essential \
    curl \
    file \
    git \
    python3-pip \
    procps \
    software-properties-common \
    tasksel \
    virtualbox

# install google chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb
rm -f google-chrome-stable_current_amd64.deb
