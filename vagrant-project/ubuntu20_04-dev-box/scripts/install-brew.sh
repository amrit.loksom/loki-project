#!/bin/bash
set -ex

apt-get update -y

### Check that Homebrew is installed and install if not
if test ! $(which brew)
then
  echo "  Installing Homebrew for you."
  yes "" | su - vagrant -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" > /tmp/homebrew-install.log
  su - vagrant -c  "$(echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/vagrant/.bashrc)"
  su - vagrant -c  "$(eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)")"
fi

# Update any existing homebrew recipes
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew update'

# Upgrade any already installed formulae
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew upgrade'

### Install my brew packages ###
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew install gcc'
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew install ansible'
# brew install awscli # not working at this moment in time
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew install warrensbox/tap/tfswitch' # if errors then: brew unlink terraform :and try install again
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew install terraform'
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew install minikube'
su - vagrant -c '/home/linuxbrew/.linuxbrew/bin/brew install kubernetes-cli'

# Update any existing homebrew recipes
# brew update

# Upgrade any already installed formulae
# brew upgrade --all

### Install my brew packages ###
# brew install wget
# brew install mpv

### Install cask ###
# brew tap phinze/homebrew-cask

### Install desired cask packages ###
# brew cask install 1password
# brew cask install alfred
# brew cask install beamer
# brew cask install betterzipql
# brew cask install caffeine
# brew cask install cakebrew
# brew cask install coconutbattery
# brew cask install coteditor
# brew cask install dropbox
# brew cask install flux
# brew cask install forklift
# brew cask install franz
# brew cask install imageoptim
# brew cask install kakapo
# brew cask install little-snitch
# brew cask install marked
# brew cask install mpv
# brew cask install qlimagesize
# brew cask install qlmarkdown
# brew cask install tg-pro
# brew cask install the-unarchiver
# brew cask install transmission
# brew cask install viscosity
# brew cask install xld

### Remove brew cruft ###
# brew cleanup

### Remove cask cruft ###
# brew cask cleanup

### Link alfred to apps ###
# brew cask alfred link