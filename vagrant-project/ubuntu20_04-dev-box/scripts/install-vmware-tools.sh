#!/bin/bash
set -e

apt update -y 

# add vmware guest tools
apt-get autoremove -y open-vm-tools 
apt-get install -y open-vm-tools-desktop
