#!/bin/bash
set -e

echo "Installing DOCKER"
# apt-get remove -y docker docker-engine docker.io containerd runc
apt-get dist-upgrade -y 
apt-get update -y
apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --batch --yes --always-trust --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list 

apt-get update -y 
apt-get install -y docker-ce docker-ce-cli containerd.io
systemctl enable docker 
systemctl start docker
chmod 666 /var/run/docker.sock
usermod -aG docker vagrant

cat <<EOF | tee /etc/docker/daemon.json 
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

systemctl restart docker
