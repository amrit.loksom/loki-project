# Provision Ubuntu20.04 VM with Desktop GUI and aws/terraform/docker/kubectl dev tools
## Tools used are as follows: 
- Virtualbox 6.1.28 
- Virtualbox-guest-additions 6.1.28
- VMware Workstation 15.5.7 (Windows OS)
- Vagrant 2.2.28 
- Vagrant plugins: 
  - vagrant-disksize (0.1.3, global) 
  - vagrant-reload (0.0.1, global) 
  - vagrant-vbguest (0.21.0, global) 

## Pre-req tools 
1. Internet Connection 
2. Virtualbox(Any OS)/VMware_Workstation(Windows OS) and virtualbox extension pack
3. Vagrant with required plugins
4. Time: This install takes a lot of time (30-45 min) due to a lot of package installed. Modify install scripts if you don't want certain tools.
There is a bash script to download the pre-req tools if you don't have them installed already. <br />
To use the pre-req installation script. <br />
Change directory to pre-req-tool-install <br />
Choose the host script and run the script. e.g. to install pre-reqs on mac OS
~~~
cd pre-req-tool-install
./mac-install-tools.sh
~~~
You can use windows-deploy-vm.ps1 script to install pre-reqs and run vagrant as such. <br />
~~~
# NOTE: copy the windows-deploy-vm.ps1 from pre-req-tool-install folder to either virtualbox or vmware directory 
# Run the following command.
./windows-deploy-vm.ps1 up
~~~

## Run the following command from vmware or virtualbox directory where the VagrantFile resides 
virtualbox
~~~
cd virtualbox
~~~

vmware
~~~
cd vmware
~~~

Then run the following command to provision VM using vagrant.

```bash
vagrant up
```

Wait for vagrant to complete the provision. 

Log in through the hypervisor local session for full desktop experience.  
~~~
Username: vagrant
Password: vagrant
~~~

To destroy the VM use the following command from the vmware/virtualbox directory.
~~~
vagrant destroy -f
~~~