﻿############################################################
############################################################
# Install Chocolatey and pre-req software                  #
############################################################
############################################################
write-host "STARTING PRE-REQ PACKAGE INSTALL"
$file = "C:\ProgramData\chocolatey\choco.exe"
$status = [System.IO.File]::Exists($file)
if ($status -eq 'true') 
{
  Write-Host "$file already exists. Not installing CHOCOLATEY"
}else{
  Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
}
# check if vmware is installed
$vmware = "C:\Program Files (x86)\VMware\VMware Workstation\vmware.exe"
$vmwstatus = [System.IO.File]::Exists($vmware)
$vbox = "C:\Program Files\Oracle\VirtualBox\VirtualBox.exe"
$vboxstatus = [System.IO.File]::Exists($vbox)
if ($vmwstatus -eq 'true') 
{
  Write-Host "$vmware already exists. Not installing HYPERVISOR application"
}elseif ($vboxstatus -eq 'true'){
  Write-Host "$vbox already exists. Not installing HYPERVISOR application"
}else{
  Write-Host "No hypervisor app found, installing virtualbox"
  choco install virtualbox -y 
}

$pkgs = "vagrant"  #,"virtualbox"
foreach ($pkg in $pkgs) {
  $status = choco list -local-only | Select-String $pkg
  if ($null -eq $status) {
  write-host "Installing $pkg ....."
  choco install $pkg -y
  }else{
  write-host "$pkg is already installed. Not installing $pkg."
  }
}

$plugins='vagrant-reload','vagrant-vbguest','vagrant-vmware-desktop','vagrant-disksize'
foreach ($plugin in $plugins) {
  $status = vagrant plugin list | Select-String $plugin
  if ($null -eq $status) {
  write-host "Installing $plugin ....."
    if ($plugin -eq "vagrant-vbguest") { 
      vagrant plugin install $plugin --plugin-version 0.21
    } elseif ($plugin -ne "vagrant-vmware-desktop") {
      vagrant plugin install $plugin
    } elseif ($vmwstatus -eq 'true' -and $plugin -eq "vagrant-vmware-desktop") {
      vagrant plugin install $plugin
    } else {
      Write-Host "Not installing vagrant plugin. Not compatible $plugin"
    }
  }else{
  write-host "$plugin is already installed. Not installing $plugin."
  }
}

write-host "PRE-REQ PACKAGE INSTALL COMPLETE"
############################################################
############################################################
# Vagrant Deploy                                           #
############################################################
############################################################
$script_path = $PSScriptRoot
write-host $script_path
Set-Location $script_path

$vagrant_command=$args[0]

if ($vagrant_command -eq "up" -or $vagrant_command -eq "Up" -or $vagrant_command -eq "UP") {
  write-host "CREATING UBUNTU20.04 VIRTUAL MACHING"
  vagrant up
}elseif ($vagrant_command -eq "destroy" -or $vagrant_command -eq "Destroy" -or $vagrant_command -eq "DESTROY") {
  write-host "DESTROYING UBUNTU20.04 VIRTUAL MACHING"
  vagrant destroy -f
}elseif ($vagrant_command -eq "provision" -or $vagrant_command -eq "Provision" -or $vagrant_command -eq "PROVISION") {
  write-host "PROVISIONING UBUNTU20.04 VIRTUAL MACHING"
  vagrant reload --provision
}else{
  write-host "please use the one of the following commands: 
  To create VM.
  ./deploy-vagrant-ubuntu.ps1 up 
  OR
  To reload config and provision VM.
  ./deploy-vagrant-ubuntu.ps1 provision 
  OR
  To delete VM and resources.
  ./deploy-vagrant-ubuntu.ps1 destroy"
}