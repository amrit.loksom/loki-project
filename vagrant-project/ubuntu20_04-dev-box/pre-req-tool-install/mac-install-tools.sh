#!/bin/bash
set -e

echo "USER input required for brew installation"

pkgs='vagrant virtualbox'

if test ! $(which brew)
then
  echo "  Installing Homebrew for you."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" > /tmp/homebrew-install.log
  # update brew
  su - $USER -c 'brew update'
else
  echo "Brew has already been installed"
fi

for pkg in $pkgs
do
  if test ! $(which $pkg)
  then
    su - $USER -c 'brew install vagrant'
    su - $USER -c 'brew install virtualbox && brew install virtualbox-extension-pack'
  else
    echo "$pkg is already installed."
  fi
done 

if test ! $(which vagrant)
then
  echo "  Installing vagrant plugins for you."
  # Install vagrant plugins
  vagrant plugin install vagrant-disksize

  vagrant plugin install vagrant-reload

  vagrant plugin install vagrant-vbguest --plugin-version 0.21
fi